﻿/*
 * GET home page.
 */
"use strict";

import express = require('express');
//import PageParams = params.PageParams;

class PageParams {
    isLoggedIn: boolean;
    messages: string[];
    title: string;
    year: number;
    userName: string;

    constructor(req, title: string) {
        // if user is authenticated in the session, carry on 
        this.title = title;
        this.isLoggedIn = req.isAuthenticated();
        var user = req.user;
        if (user && user.local) {
            this.userName = req.user.local.email;
        }

        year: new Date().getFullYear();

        var msg = req.flash('loginMessage');
        this.messages = [];

        if (msg) {
            this.messages.push(msg);
        }
        msg = req.flash('errorMessage');
        if (msg) {
            this.messages.push(msg);
        }
        msg = req.flash('general');
        if (msg) {
            this.messages.push(msg);
        }
    }
}

export function index(req: express.Request, res: express.Response) {
    res.render('index', { params: new PageParams(req, 'Express')});
};

export function about(req: express.Request, res: express.Response) {
    res.render('about', { params: new PageParams(req, 'About') });
};

export function contact(req: express.Request, res: express.Response) {
    res.render('contact', { params: new PageParams(req, 'Contact') });
};

export function login(req: express.Request, res: express.Response) {
    res.render('login', { params: new PageParams(req, 'Login') });
};

export function signup(req: express.Request, res: express.Response) {
    res.render('signup', { params: new PageParams(req, 'Sign up') });
};

export function profile(req: express.Request, res: express.Response) {
    res.render('profile', { params: new PageParams(req, 'Profile') });
};


