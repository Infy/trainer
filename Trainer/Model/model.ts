﻿// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
    name: String,
    local: {
        email: String,
        password: String,
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    }

});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);



//import mongoose = require('mongoose');
//var bcrypt = require('bcrypt-nodejs');

//export interface IUser extends mongoose.Document {
//    //name?: string;
//    local: {
//        email: string,
//        password: string,
//    },
//    facebook: {
//        id: string,
//        token: string,
//        email: string,
//        name: string
//    },
//    twitter: {
//        id: string,
//        token: string,
//        displayName: string,
//        username: string
//    },
//    google: {
//        id: string,
//        token: string,
//        email: string,
//        name: string
//    }
//};

//export const UserSchema = new mongoose.Schema({
//    name: String,
//    local: {
//        email: String,
//        password: String,
//    },
//    facebook: {
//        id: String,
//        token: String,
//        email: String,
//        name: String
//    },
//    twitter: {
//        id: String,
//        token: String,
//        displayName: String,
//        username: String
//    },
//    google: {
//        id: String,
//        token: String,
//        email: String,
//        name: String
//    }
//});
//// methods ======================
//// generating a hash
//UserSchema.methods.generateHash = function (password) {
//    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
//};

//// checking if password is valid
//UserSchema.methods.validPassword = function (password) {
//    return bcrypt.compareSync(password, this.local.password);
//};

//// create the model for users and expose it to our app
//// ReSharper disable once InconsistentNaming
//export const User = mongoose.model<IUser>('User', UserSchema);